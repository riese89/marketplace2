<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Auth::routes();
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/', ['as' => 'Order.Create', 'uses' =>'OrderController@create']);

//Личный кабинет

//Route::get('/lk', ['as' => 'Lk.Index', 'uses' =>'LkController@index']);
//Route::get('/lk/{id}', ['as' => 'OperatingOrder.Show', 'uses' => 'OrderController@show']);

//Каталог сред

Route::get('/catalogenv', ['as' => 'Env.Index', 'uses' => 'EnvController@index']);
Route::get('/catalogenv/edit/{id}', ['as' => 'OperatingForm.Edit', 'uses' => 'EnvController@Edit']);
Route::get('/catalogenv/destroy/{id}', ['as' => 'OperatingForm.Destroy', 'uses' => 'EnvController@Destroy']);
Route::get('/catalogenv/update/{id}', ['as' => 'OperatingForm.Update', 'uses' => 'EnvController@Update']);
Route::get('/catalogenv/createform', ['as' => 'OperatingForm.Create', 'uses' => 'EnvController@Create']);
Route::get('/catalogenv/store', ['as' => 'OperatingForm.Store', 'uses' => 'EnvController@Store']);

//Заказы

Route::get('/catalogorders', ['as' => 'Order.Index', 'uses' =>'OrderController@index']);
Route::get('/createorder', ['as' => 'Order.Create', 'uses' => 'OrderController@create']);
Route::get('/createorder/store', ['as' => 'OperatingOrder.Store', 'uses' => 'OrderController@store']);
Route::get('/createorder/change', ['as' => 'OperatingOrder.Change', 'uses' => 'OrderController@change']);
Route::get('/createorder/destroy/{id}', ['as' => 'OperatingOrder.Destroy', 'uses' => 'OrderController@Destroy']);
Route::get('/showorder/{id}', ['as' => 'OperatingOrder.Show', 'uses' => 'OrderController@show']);

//Каталог ОС

Route::get('/catalogos', ['as' => 'Os.Index', 'uses' =>'OsController@index']);
Route::get('/catalogos/edit/{id}', ['as' => 'OperatingOs.Edit', 'uses' => 'OsController@Edit']);
Route::get('/catalogos/destroy/{id}', ['as' => 'OperatingOs.Destroy', 'uses' => 'OsController@Destroy']);
Route::get('/catalogos/update/{id}', ['as' => 'OperatingOs.Update', 'uses' => 'OsController@Update']);
Route::get('/catalogos/createos', ['as' => 'OperatingOs.Create', 'uses' => 'OsController@Create']);
Route::get('/catalogos/store', ['as' => 'OperatingOs.Store', 'uses' => 'OsController@Store']);

//Каталог SLA

Route::get('/catalogsla', ['as' => 'OperatingSla.Index', 'uses' => 'SlaController@index']);
Route::get('/catalogsla/edit/{id}', ['as' => 'OperatingSla.Edit', 'uses' => 'SlaController@Edit']);
Route::get('/catalogsla/destroy/{id}', ['as' => 'OperatingSla.Destroy', 'uses' => 'SlaController@Destroy']);
Route::get('/catalogsla/update/{id}', ['as' => 'OperatingSla.Update', 'uses' => 'SlaController@Update']);
Route::get('/catalogsla/createsla', ['as' => 'OperatingSla.Create', 'uses' => 'SlaController@Create']);
Route::get('/catalogsla/store', ['as' => 'OperatingSla.Store', 'uses' => 'SlaController@Store']);
Route::get('/catalogserver', ['as' => 'OperatingSla.Index', 'uses' =>'ServerController@index']);

//Каталог серверов

Route::get('/catalogserver/edit/{id}', ['as' => 'OperatingServer.Edit', 'uses' => 'ServerController@Edit']);
Route::get('/catalogserver/destroy/{id}', ['as' => 'OperatingServer.Destroy', 'uses' => 'ServerController@Destroy']);
Route::get('/catalogserver/update/{id}', ['as' => 'OperatingServer.Update', 'uses' => 'ServerController@Update']);
Route::get('/catalogserver/createserver', ['as' => 'OperatingServer.Create', 'uses' => 'ServerController@Create']);
Route::get('/catalogserver/store', ['as' => 'OperatingServer.Store', 'uses' => 'ServerController@Store']);

//Каталог типов памяти

Route::get('/typememory', ['as' => 'OperatingTypeMemory.Index', 'uses' => 'TypeMemoryController@index']);
Route::get('/typememory/edit/{id}', ['as' => 'OperatingTypeMemory.Edit', 'uses' => 'TypeMemoryController@Edit']);
Route::get('/typememory/destroy/{id}', ['as' => 'OperatingTypeMemory.Destroy', 'uses' => 'TypeMemoryController@Destroy']);
Route::get('/typememory/update/{id}', ['as' => 'OperatingTypeMemory.Update', 'uses' => 'TypeMemoryController@Update']);
Route::get('/typememory/createsla', ['as' => 'OperatingTypeMemory.Create', 'uses' => 'TypeMemoryController@Create']);
Route::get('/typememory/store', ['as' => 'OperatingTypeMemory.Store', 'uses' => 'TypeMemoryController@Store']);

//Каталог конфигураций

Route::get('/configuration', ['as' => 'OperatingConfiguration.Index', 'uses' => 'ConfigurationController@index']);
Route::get('/configuration/edit/{id}', ['as' => 'OperatingConfiguration.Edit', 'uses' => 'ConfigurationController@Edit']);
Route::get('/configuration/destroy/{id}', ['as' => 'OperatingConfiguration.Destroy', 'uses' => 'ConfigurationController@Destroy']);
Route::get('/configuration/createconfig', ['as' => 'OperatingConfiguration.Create', 'uses' => 'ConfigurationController@Create']);
Route::get('/configuration/store/{id}', ['as' => 'OperatingConfiguration.Store', 'uses' => 'ConfigurationController@Store']);
//Route::get('/configuration/createconfig', ['as' => 'OperatingConfiguration.Edit', 'uses' => 'ConfigurationController@Create']);


//Корзина

Route::get('/bucket', ['as' => 'OperatingBucket.Index', 'uses' => 'BucketController@index']);

//Тестирование

Route::get('test', ['as' => 'Test', 'uses' => 'TestController@index']);
Route::get('save', ['as' => 'Test.Save', 'uses' => 'TestController@save']);
Route::get('city', ['as' => 'Test.City', 'uses' => 'TestController@city']);
//Route::get('test', ['as' => 'IndexTest', 'uses' => 'TestController@index_test']);
/*DB::listen(function($query) {
    var_dump($query->sql, $query->bindings);
});*/




