<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class sla extends Model
{
    protected $fillable = ['level', 'updated_at', 'created_at'];

    public function Configuration() {
        return $this->belongsToMany('App\Model\Configuration');
    }
}
