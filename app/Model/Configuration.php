<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    //
    protected $fillable = ['os', 'performance', 'server', 'typememory', 'environment', 'cost', 'sla','memorydisk','ram','countcoresvcpu', 'updated_at', 'created_at'];

    public function Bucket_item() {
        return $this->belongsToMany('App\Model\Bucket_item');
    }
    public function Performance()
    {
        return $this->belongsToMany('App\Model\Performance', 'configuration_performances');
        //return $this->hasOne('App\Model\Performance');
    }
    public function Server()
    {
        return $this->belongsToMany('App\Model\Server', 'configuration_servers');
    }
    public function TypeMemory()
    {
        return $this->belongsToMany('App\Model\TypeMemory', 'configuration_type_memories');
    }
    public function Environment()
    {
        return $this->belongsToMany('App\Model\Environment', 'configuration_environments');
    }
    public function Sla()
    {
        return $this->belongsToMany('App\Model\Sla', 'configuration_slas');
    }
    public function Order() {
        return $this->belongsToMany('App\Model\Order');
    }
}
