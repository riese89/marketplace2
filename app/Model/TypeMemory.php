<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TypeMemory extends Model
{
    //
    protected $fillable = ['type', 'description', 'updated_at', 'created_at'];

        public function Configuration() {
            return $this->belongsToMany('App\Model\Configuration');
        }
}
