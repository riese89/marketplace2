<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = ['nameServer', 'descriptionServer', 'costServer', 'updated_at', 'created_at'];

    public function Configuration() {
        return $this->belongsToMany('App\Model\Configuration');
    }

}
