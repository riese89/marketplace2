<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 28.10.2019
 * Time: 22:42
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class configuration_sla extends Model
{
    protected $fillable = ['configuration_id','slas_id'];
}