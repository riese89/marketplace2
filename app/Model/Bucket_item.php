<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 12.10.2019
 * Time: 0:00
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class Bucket_item extends Model
{
    protected $fillable = ['orderName', 'bucket_id', 'oss', 'price', 'config_id', 'quantity', 'updated_at', 'created_at'];

    public function Bucket()    {
        return $this->belongsTo('App\Model\Bucket');
    }
    public function Order()    {
        return $this->belongsTo('App\Model\Order');
    }

    public function Configuration()
    {
        return $this->belongsToMany('App\Model\Configuration','bucket_item_configurations','bucket_item_id','configuration_id');
    }
    public function Os()
    {
        return $this->belongsToMany('App\Model\Os', 'bucket_item_os','bucket_item_id','oss_id');
    }
    /*public function Bucket_item() {
        return $this->belongsToMany('App\Model\Order','buckets');
    }*/

}