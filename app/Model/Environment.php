<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Environment extends Model
{
    protected $fillable = ['env', 'description', 'updated_at', 'created_at'];

    public function Configuration() {
        return $this->belongsToMany('App\Model\Configuration');
    }
}
