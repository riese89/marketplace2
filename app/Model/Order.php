<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected  $primaryKey = 'id';
    protected $fillable = ['ordername','configuration', 'sum', 'updated_at', 'created_at'];

    public function Bucket()
    {
        return $this->hasOne('App\Model\Bucket');
    }
    public function Bucket_item()
    {
        return $this->hasMany('App\Model\Bucket_item');
    }
    public function Configuration()
    {
        return $this->belongsToMany('App\Model\Configuration','bucket_items','order_id','config_id');
    }
    /*public function Bucket_item() {
        return $this->hasOne('App\Model\Bucket_item');
    }*/
}
