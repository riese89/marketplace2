<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 27.10.2019
 * Time: 16:27
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class configuration_type_memories extends Model
{
    //
    protected $fillable = ['configuration_id', 'type_memory_id'];
}
