<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    //
    protected $fillable = ['level', 'updated_at', 'created_at'];

    public function Configuration() {
        return $this->belongsToMany('App\Model\Configuration');
        //return $this->belongsTo('App\Model\Configuration','id','performance');
    }

}
