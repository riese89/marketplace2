<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Os extends Model
{
    //
    protected $fillable = ['nameOs','descriptionOs', 'updated_at', 'created_at'];

    public function Bucket_item() {
        return $this->belongsToMany('App\Model\Bucket_item');
    }
}
