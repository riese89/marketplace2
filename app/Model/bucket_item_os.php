<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 30.10.2019
 * Time: 23:17
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class bucket_item_os extends Model
{
    protected $fillable = ['bucket_item_id', 'oss_id'];
}