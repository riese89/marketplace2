<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 11.10.2019
 * Time: 23:59
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Bucket extends Model
{
    protected $fillable = ['user_id', 'total', 'updated_at', 'created_at', 'status'];

    public function Order() {
        return $this->belongsTo('App\Model\Order');
    }

    public function Bucket_item()
    {
        return $this->hasMany('App\Model\Bucket_item');
    }

}