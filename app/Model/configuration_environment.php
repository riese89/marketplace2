<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 27.10.2019
 * Time: 19:00
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class configuration_environment extends Model
{
    //
    protected $fillable = ['configuration_id', 'environment_id'];
}
