<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 27.10.2019
 * Time: 16:01
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class configuration_server extends Model
{
    protected $fillable = ['configuration_id', 'server_id', 'updated_at','created_at'];

}