<?php

namespace App\Http\Controllers;


/*use App\Model\bucket_item_os;
use Illuminate\Http\Request;
use App\Model\Configuration;
use App\Model\Os;
use App\Model\Order;
use App\Classes\asArrayEnvConf;
use Illuminate\Support\Facades\DB;
use App\Model\Bucket;
use App\Model\Bucket_item;
use App\Model\bucket_item_configuration;*/
use Illuminate\{
    Http\Request,
    Support\Facades\DB
};
use App\Classes\asArrayEnvConf;

use App\Model\{
    Configuration, Environment, Order, Os, Bucket, bucket_item_configuration, Bucket_item, bucket_item_os
};


class OrderController extends Controller
{
    //
    public function show($id)
    {
        $orders = Order::find($id);
        $confs = $orders->Configuration;
        $bucket_items = $orders->Bucket_item;
        $bucket = $orders->Bucket;
        return view('/Order/ShowOrder',compact('confs','bucket','bucket_items'));
    }

    public function create()
    {
        $envs = Configuration::select('environment')->distinct('environment')->get();
        /*echo "<pre>";
        foreach ($envs as $env) {
            echo $env['environment']."<br>";
        }
        echo "</pre>";*/
        foreach ($envs as $env)   {
            $nameEnvs[] = Environment::find($env['environment']);
        }
        $configurations = Configuration::get();
        $oss = Os::pluck('nameOs','id');
        foreach ($configurations as $configuration)   {
            $confs[] = Configuration::find($configuration['id']);
        }
        $bucket_items = Bucket_item::where('order_id',null)->get();
        $buckets = Bucket::where('order_id', null)->get();

        return view('/Order/CreateOrder', compact('nameEnvs', 'confs','oss','bucket_items','buckets'));
        //return view('/Test', compact('configurations','oss','bucket'));
    }

    public function store(Request $request)
    {
        //echo "Создаем заказ ".$request->get("id");
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $orders = new Order();
        $orders->bucket_id = $request->get("id");
        $orders->save();
        $orders = Order::select('id')->where('bucket_id', $request->get('id'))->get()[0]['id'];
        $buckets = Bucket::find($request->get("id"));
        $buckets->order_id = $orders;
        $buckets->status = 1;
        $buckets->save();
        DB::table('bucket_items')->where('order_id',null)->update(['order_id' => $orders]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        return view('/Order/MessageOrder',compact('orders'));
    }
    public function change(Request $request)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        for ($inkr = 0; $request->get("oss$inkr"); $inkr++) {
            if ($request->get("configuration$inkr") || $request->get("plus$inkr") || $request->get("minus$inkr")) {
                $buckets = Bucket::where('status', 0)->get();
                if (count($buckets) == 0) {
                    //Такого нет
                    $buckets = new Bucket();
                    $buckets->user_id = 1;
                    $buckets->total = 0;
                    $buckets->status = 0;
                    $buckets->save();
                    $bucket_items = new Bucket_item();
                    $buckets = Bucket::where('status', 0)->get();
                    $bucket_items->bucket_id = $buckets[0]['id'];
                    $bucket_items->orderName = sprintf('%03d', (count(Order::where('created_at', ">=", date("Y-m-d"))->get()) + 1)) . '-' . date("Y-m-d");
                    $bucket_items->oss = $request->get("oss$inkr");
                    if ($request->get("plus$inkr")) {
                        $bucket_items->config_id = $request->get("plus$inkr");
                        $bucket_items->price = Configuration::select('cost')->where('id', $request->get("plus$inkr"))->get()[0]['cost'];
                    } else {
                        $bucket_items->config_id = $request->get("configuration$inkr");
                        $bucket_items->price = Configuration::select('cost')->where('id', $request->get("configuration$inkr"))->get()[0]['cost'];
                    }
                    $bucket_items->quantity = 1;
                    $bucket_items->save();
                    $bucket_items = Bucket_item::where('bucket_id', $buckets[0]['id'])->get();
                    $buckets = Bucket::find($bucket_items[0]['bucket_id']);
                    $buckets->total = $bucket_items[0]['price'] * $bucket_items[0]['quantity'];
                    $buckets->save();
                    $bc = new bucket_item_configuration();
                    $bc->bucket_item_id = $bucket_items[0]['id'];
                    $bc->configuration_id = $bucket_items[0]['config_id'];
                    $bc->save();
                    $bo = new bucket_item_os();
                    $bo->bucket_item_id = $bucket_items[0]['id'];
                    $bo->oss_id = $request->get("oss$inkr");
                    $bo->save();
                }
                else {
                    $buckets = Bucket::where('status',0)->get()[0]['id'];
                    if ($request->get("configuration$inkr")) {
                        $bucket_items = Bucket::find($buckets)->bucket_item()
                            ->where('config_id',$request->get("configuration$inkr"))
                            ->get();
                    }
                    if ($request->get("plus$inkr")) {
                        $bucket_items = Bucket::find($buckets)->bucket_item()
                            ->where('config_id',$request->get("plus$inkr"))
                            ->get();
                    }
                    if ($request->get("minus$inkr")) {
                        $bucket_items = Bucket::find($buckets)->bucket_item()
                            ->where('config_id',$request->get("minus$inkr"))
                            ->get();
                    }
                    if (count($bucket_items))   {
                        if ($request->get("plus$inkr") || $request->get("minus$inkr")) {
                            if ($request->get("plus$inkr")) {
                                $bucket_items = Bucket_item::find(Bucket_item::select('id')
                                    ->where('config_id', '=', $request->get("plus$inkr"))
                                    ->where('bucket_id', '=', $buckets)
                                    ->get()[0]['id']);
                                $bucket_id = $bucket_items['bucket_id'];
                                $bucket_items->oss = $request->get("oss$inkr");
                                $bucket_items->quantity++;
                                $bucket_items->save();
                            }
                            if ($request->get("minus$inkr")) {
                                $bucket_items = Bucket_item::find(Bucket_item::select('id')
                                    ->where('config_id', '=', $request->get("minus$inkr"))
                                    ->where('bucket_id', '=', $buckets)
                                    ->get()[0]['id']);
                                $bucket_id = $bucket_items['bucket_id'];
                                $bucket_items->oss = $request->get("oss$inkr");
                                $bucket_items->quantity--;
                                if ($bucket_items->quantity == 0)   {
                                    $bucket_items->delete();
                                }
                                else    {
                                $bucket_items->save();
                                }
                            }
                        }
                        else    {
                        if ($request->get("configuration$inkr")) {
                            $bucket_items = Bucket_item::find(Bucket_item::select('id')
                                ->where('config_id', '=', $request->get("configuration$inkr"))
                                ->where('bucket_id', '=', $buckets)
                                ->get()[0]['id']);
                            $bucket_id = $bucket_items['bucket_id'];
                            $bucket_items->oss = $request->get("oss$inkr");
                            $bucket_items->save();
                        }
                        }
                        $buckets = Bucket::find($bucket_id);
                        $bucket_items = Bucket_item::select('quantity', 'price')
                            ->where('quantity', '>', 0)
                            ->where('bucket_id', $bucket_id)->get();
                        $buckets->total = 0;
                        foreach ($bucket_items as $bucket_item) {
                            $buckets->total += $bucket_item['quantity'] * $bucket_item['price'];
                        }
                        $buckets->save();

                    } else {
                        $buckets = Bucket::where('status', 0)->get();
                        $bucket_items = Bucket_item::where('bucket_id', $buckets[0]['id'])->first();
                        $orderName = $bucket_items->orderName;
                        $bucket_items = new Bucket_item();
                        $bucket_items->bucket_id = $buckets[0]['id'];
                        $bucket_items->orderName = $orderName;
                        $bucket_items->oss = $request->get("oss$inkr");
                        if ($request->get("plus$inkr")) {
                            $bucket_items->config_id = $request->get("plus$inkr");
                            $bi_ci = $request->get("plus$inkr");
                        } elseif ($request->get("configuration$inkr")) {
                            $bucket_items->config_id = $request->get("configuration$inkr");
                            $bi_ci = $request->get("configuration$inkr");
                        } else {
                            $bucket_items->config_id = $request->get("minus$inkr");
                            $bi_ci = $request->get("minus$inkr");
                        }
                        $bucket_items->price = Configuration::select('cost')->where('id', $bucket_items->config_id)->get()[0]['cost'];
                        $bucket_items->quantity = 1;
                        $buckets = Bucket::find($bucket_items->bucket_id);
                        $buckets->total += $bucket_items->price * $bucket_items->quantity;
                        $buckets->save();
                        $bucket_items->save();
                        $bc = new bucket_item_configuration();
                        $bc->bucket_item_id = DB::table('bucket_items')->latest('id')->first()->id;
                        $bc->configuration_id = $bi_ci;
                        $bc->save();
                        $bo = new bucket_item_os();
                        $bo->bucket_item_id = DB::table('bucket_items')->latest('id')->first()->id;
                        $bo->oss_id = $request->get("oss$inkr");
                        $bo->save();
                    }
                }
            }

        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        return redirect(route('Order.Create'));

    }

    public function index()
    {
        $orders = Order::get();
        foreach ($orders as $order) {
            $buckets[] = Bucket::where('order_id', $order['id'])->get();
        }

        return view('/Order/CatalogOrder', compact( 'buckets'));
    }

    public function destroy($id)
    {
        $orders = Order::find($id);
        $orders->delete();
        return redirect('/catalogorders');
    }


}