<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Performance;

class PerformanceController extends Controller
{
    //
    public function index()
    {
        $performances = Performance::get();
        return view('/Performance/CatalogPerformance', compact('performances'));
    }

    public function create()
    {
        //
        return view('/Performance/CreatePerformance');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $performance = new Performance([
            'level' => $request->get('level'),
        ]);
        $performance->save();
        return redirect('/performance');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $performances = Performance::where('id', $id)->get();
        return view('/Performance/EditPerformance', compact('performances'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $performance = Performance::find($id);
        $performance->level = $request->get('level');
        $performance->save();
        return redirect('/performance');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $performance = Performance::find($id);
        $performance->delete();
        return redirect('/performance');
    }

}
