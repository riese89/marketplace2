<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Server;
use App\Model\Order;

class ServerController extends Controller
{
    //
    public function index()
    {
        $servers = Server::get();
        return view('/Server/CatalogServer', compact('servers'));
    }

    public function create()
    {
        //
        return view('/Server/CreateServer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $servers = new Server([
            'nameServer' => $request->get('nameServer'),
            'descriptionServer' => $request->get('descriptionServer'),
            'costServer' => $request->get('costServer'),

        ]);
        $servers->save();
        return redirect('/catalogserver');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $servers = Server::where('id', $id)->get();
        return view('/Server/EditServer', compact('servers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servers = Server::find($id);
        $servers->nameServer = $request->get('nameServer');
        $servers->descriptionServer = $request->get('descriptionServer');
        $servers->costServer = $request->get('costServer');
        $servers->save();
        return redirect('/catalogserver');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $servers = Server::find($id);
        $orders = Order::where('server', $id)->get();
        if (count($orders) != 0) {
            echo "Сервер задействован в заказах";
        } else {
            $servers->delete();
            return redirect('/catalogserver');
        }
    }
}
