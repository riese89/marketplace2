<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TypeMemory;

class TypeMemoryController extends Controller
{
    //
    public function index()
    {
        $types = TypeMemory::get();
        return view('/TM/CatalogTypeMemory', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('/TM/CreateTypeMemory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $types = new TypeMemory([
            'type' => $request->get('type'),
            'description' => $request->get('description'),
        ]);
        $types->save();
        return redirect('/typememory');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $types = TypeMemory::where('id', $id)->get();
        return view('/TM/EditTypeMemory', compact('types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $types = TypeMemory::find($id);
        $types->type = $request->get('type');
        $types->description = $request->get('description');
        $types->save();
        return redirect('/typememory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $types = TypeMemory::find($id);
        $types->delete();
        return redirect('/typememory');
    }
}
