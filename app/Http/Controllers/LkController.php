<?php

namespace App\Http\Controllers;
use App\Model\Order;
use App\Classes\asArrayEnvConf;
use App\Model\Bucket;
use App\Model\Bucket_item;


class LkController extends Controller
{
    public function index()
    {
        $buckets = Bucket::select('id','total','order_id')->where('order_id',"<>",null)->get();
        return view('/Lk/lk', compact( 'buckets'));
    }
}
