<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Environment;

class EnvController extends Controller
{

    public function index()
    {
        $environments = Environment::get();
        return view('/Env/CatalogEnv', compact('environments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('/Env/CreateForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $environment = new Environment([
            'env' => $request->get('nameEnv'),
            'description' => $request->get('DescrEnv'),
        ]);
        $environment->save();
        return redirect('/catalogenv');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $envs = Environment::where('id', $id)->get();
        return view('/Env/EditForm', compact('envs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $environment = Environment::find($id);
        $environment->Env = $request->get('nameEnv');
        $environment->Description = $request->get('DescrEnv');
        $environment->save();
        return redirect('/catalogenv');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $environment = Environment::find($id);
        $environment->delete();
        return redirect('/catalogenv');
    }

}
