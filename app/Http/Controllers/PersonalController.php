<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 15.08.2019
 * Time: 15:25
 */

namespace App\Http\Controllers;


use App\Post;
use Illuminate\Support\Facades\DB;

class PersonalController extends Controller
{
    public function index()
    {
       // $posts = DB::table('posts')->get();
        $posts = Post::all();
        //return $posts;
        return view('post', compact('posts'));

        //return 'ok';
    }

}