<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SLA;

class SlaController extends Controller
{
    //
    public function index()
    {
        $slas = Sla::get();
        return view('/SLA/CatalogSla', compact('slas'));
    }
    public function create()
    {
        //
        return view('/SLA/CreateSla');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $environment = new Sla([
            'level' => $request->get('level'),
        ]);
        $environment->save();
        return redirect('/catalogsla');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $slas = Sla::where('id', $id)->get();
        return view('/SLA/EditSla', compact('slas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sla = Sla::find($id);
        $sla->level = $request->get('level');
        $sla->save();
        return redirect('/catalogsla');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sla = Sla::find($id);
        $sla->delete();
        return redirect('/catalogsla');
    }
    protected static function priv($a) {
        $a += 3142424;
        return $a;
    }
    public static function ii($a)  {
        return SlaController::priv($a);
    }

}
