<?php
/**
 * Created by PhpStorm.
 * User: usser
 * Date: 13.11.2019
 * Time: 10:41
 */

//стэк

$stack = new SplStack();

// добавляем элемент в стек
$stack->push('1');
$stack->push('2');
$stack->push('3');

echo $stack->count(); // 3 число
echo $stack->top(); // 3 верх
echo $stack->bottom(); // 1 низ
echo $stack->serialize(); // i:6;:s:1:"1";:s:1:"2";:s:1:"3"; преобразование в поток байтов

// извлекаем элементы из стека
echo $stack->pop(); // 3
echo $stack->pop(); // 2
echo $stack->pop(); // 1

//очередь

$queue = new SplQueue();

$queue->setIteratorMode(SplQueue::IT_MODE_DELETE);

$queue->enqueue('one'); //добавление
$queue->enqueue('two');
$queue->enqueue('qwerty');

$queue->dequeue(); //удаление
$queue->dequeue();

echo $queue->top(); // qwerty

//кучи древовидная структура, где каждый след элемент больше или меньше предыдущего

$heap = new SplMaxHeap(); //от большего к меньшему
$heap->insert('111');
$heap->insert('666');
$heap->insert('777');

echo $heap->extract(); // 777
echo $heap->extract(); // 666
echo $heap->extract(); // 111

$heap = new SplMinHeap(); //от меньшего к большему
$heap->insert('111');
$heap->insert('666');
$heap->insert('777');

echo $heap->extract(); // 111
echo $heap->extract(); // 666
echo $heap->extract(); // 777


//очередь с приоритетами

$queue = new SplPriorityQueue();
$queue->setExtractFlags(SplPriorityQueue::EXTR_DATA); // получаем только значения элементов

$queue->insert('Q', 1);
$queue->insert('W', 2);
$queue->insert('E', 3);
$queue->insert('R', 4);
$queue->insert('T', 5);
$queue->insert('Y', 6);

$queue->top();

while($queue->valid())
{
    echo $queue->current();
    $queue->next();
}

//массив с фиксируемым числом эдементов

$a = new SplFixedArray(10000);
$count = 100000;

for($i =0; $i<$count; $i++)
{
    $a[$i] = $i;

    if ($i==9999) $a->setSize(100000);
}

//хранилище

$s = new SplObjectStorage(); // создаем хранилище

$o1 = new StdClass;
$o2 = new StdClass;
$o3 = new StdClass;

$s->attach($o1); // прикрепляем к хранилищу объект
$s->attach($o2);

var_dump($s->contains($o1)); // bool(true)
var_dump($s->contains($o2)); // bool(true)
var_dump($s->contains($o3)); // bool(false)

$s->detach($o2); // открепляем объект от хранилища

var_dump($s->contains($o1)); // bool(true)
var_dump($s->contains($o2)); // bool(false)
var_dump($s->contains($o3)); // bool(false)


//двусвяязанный список

class DoublyLinkedList {
    private $start = null;
    private $end = null;

    public function add(Element $element) {
        //if this is the first element we've added, we need to set the start
        //and end to this one element
        if($this->start === null) {
            $this->start = $element;
            $this->end = $element;
            return;
        }

        //there were elements already, so we need to point the end of our list
        //to this new element and make the new one the end
        $this->end->setNext($element);
        $element->setPrevious($this->end);
        $this->end = $element;
    }

    public function getStart() {
        return $this->start;
    }

    public function getEnd() {
        return $this->end;
    }
}

class Element {
    private $prev;
    private $next;
    private $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function setPrevious(Element $element) {
        $this->prev = $element;
    }

    public function setNext(Element $element) {
        $this->next = $element;
    }

    public function setData($data) {
        $this->data = $data;
    }
}

