<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Os;

class OsController extends Controller
{
    //
    public function index()
    {
        $oss = Os::get();
        return view('/Os/CatalogOs', compact('oss'));
    }
    public function create()
    {
        //
        return view('/Os/CreateOs');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $os = new Os([
            'nameOs' => $request->get('nameOs'),
            'descriptionOs' => $request->get('descriptionOs'),
        ]);
        $os->save();
        return redirect('/catalogos');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        //
        $oss = Os::where('id', $id)->get();
        return view('/Os/EditOs', compact('oss'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $os = Os::find($id);
        $os->nameOs = $request->get('nameOs');
        $os->descriptionOs = $request->get('descriptionOs');
        $os->save();
        return redirect('/catalogos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $os = Os::find($id);
        $os->delete();
        return redirect('/catalogos');
    }

}
