<?php

namespace App\Http\Controllers;

use Illuminate\{
    Http\Request,
    Support\Facades\DB
};

use App\Model\{
    Configuration,
    Server,
    Sla,
    Environment,
    Performance,
    TypeMemory,
    configuration_type_memories,
    configuration_performance,
    configuration_server,
    configuration_environment,
    configuration_sla
};

class ConfigurationController extends Controller
{
    public function index()
    {
        $configurations = Configuration::get();
        foreach ($configurations as $configuration) {
            $confs[] = Configuration::find($configuration->id);
        }
        return view('/Configuration/CatalogConfiguration', compact('confs'));
    }

    public function create()
    {
        /** @var Server $servers */
        $servers = Server::pluck('nameServer', 'id');
        $slas = Sla::pluck('level', 'id');
        /** @var Environment $envs */
        $envs = Environment::pluck('env', 'id');
        $typeMemories = TypeMemory::pluck('type', 'id');
        $performances = Performance::pluck('level', 'id');
        $confs = null;
        return view('/Configuration/EditConfiguration', compact('confs','envs', 'slas', 'servers', 'typeMemories', 'performances'));
        //return view('/Configuration/CreateConfiguration', compact('servers', 'slas', 'envs', 'typeMemories', 'performances'));
    }

    public function store(Request $request, $id)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $configurations = Configuration::find($id);
        if (empty($configurations)) {
            $configurations = new Configuration();
        }
        $configurations->sla = $request->post('slas');
        $configurations->performance = $request->post('performances');
        $configurations->server = $request->post('servers');
        $configurations->typememory = $request->post('typememories');
        $configurations->environment = $request->post('envs');
        $configurations->cost = $request->post('cost');
        $configurations->memorydisk = $request->post('memoryDisk');
        $configurations->ram = $request->post('ram');
        $configurations->countcoresvcpu = $request->post('countCoresVcpu');
        $configurations->save();
        $configurations = Configuration::orderBy('created_at', 'desc')->first();
        $this->change_config($configurations);
        return redirect('/configuration');
    }

    /**
     * @param $id int
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Edit($id)
    {
        //
        $confs = Configuration::find($id);
        $envs = Environment::pluck('env', 'id');
        $slas = Sla::pluck('level', 'id');
        $servers = Server::pluck('nameServer', 'id');
        $typeMemories = TypeMemory::pluck('type', 'id');
        $performances = Performance::pluck('level', 'id');
        return view('/Configuration/EditConfiguration', compact('confs', 'envs', 'slas', 'servers', 'typeMemories', 'performances'));
    }

    public function destroy($id)
    {
        //
        $environment = Configuration::find($id);
        $environment->delete();
        return redirect(route('OperatingConfiguration.Index'));
    }

    public function change_config($configuration)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $confperfs = configuration_performance::where('configuration_id', $configuration->id)->first();
        if (!$confperfs) {
            $confperfs = new configuration_performance();
            $confperfs->configuration_id = $configuration->id;
        }
        $confperfs->performance_id = $configuration->performance;
        $confperfs->save();

        $confservs = configuration_server::where('configuration_id', $configuration->id)->first();
        if (!$confservs) {
            $confservs = new configuration_server();
            $confservs->configuration_id = $configuration->id;
        }
        $confservs->server_id = $configuration->server;
        $confservs->save();

        $confTM = configuration_type_memories::where('configuration_id', $configuration->id)->first();
        if (!$confTM) {
            $confTM = new configuration_type_memories();
            $confTM->configuration_id = $configuration->id;
        }
        $confTM->type_memory_id = $configuration->typememory;
        $confTM->save();

        $confenvs = configuration_environment::where('configuration_id', $configuration->id)->first();
        if (!$confenvs) {
            $confenvs = new configuration_environment();
            $confenvs->configuration_id = $configuration->id;
        }
        $confenvs->environment_id = $configuration->environment;
        $confenvs->save();

        $confslas = configuration_sla::where('configuration_id', $configuration->id)->first();
        if (!$confslas) {
            $confslas = new configuration_sla();
            $confslas->configuration_id = $configuration->id;
        }
        $confslas->sla_id = $configuration->sla;
        $confslas->save();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}