@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник ОС</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>ОС</th>
            <th>Описание</th>
            <th>Действие</th>
            @foreach($oss as $os)
                <tr>
                    <td>{{$os->nameOs}}</td>
                    <td>{{$os->descriptionOs}}</td>
                    <td><a href="{{route("OperatingOs.Edit",['id' => $os->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingOs.Destroy", ['id' => $os->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingOs.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить ОС</button></a>

@endsection