@extends('layouts.MarketPlace')

@section('content')
    <h1>Изменение ОС</h1>
    @foreach($oss as $os)
        <form action = "{{route("OperatingOs.Update",['id' => $os->id])}}" method = "GET">
            Название<br>
            <div class="input-group mb-3">
                <input type="text" name = "nameOs" value = "{{$os->nameOs}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            Описание<br>
            <div class="input-group mb-3">
                <input type="text" name = "descriptionOs" value = "{{$os->descriptionOs}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type = "submit">Сохранить</button>
        </form>
    @endforeach
@endsection