@extends('layouts.MarketPlace')

@section('content')
    <h1> Создание ОС</h1>
    <form action = "{{route("OperatingOs.Store")}}" method = "GET">
        Название ОС<br>
        <div class="input-group mb-3">
            <input type="text" name = "nameOs" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        Описание ОС<br>
        <div class="input-group mb-3">
            <input type="text" name = "descriptionOs" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <button type = "submit">Создать</button>
    </form>
@endsection