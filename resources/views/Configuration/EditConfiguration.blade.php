@extends('layouts.MarketPlace')

@section('content')

    @if(isset($confs))
        <h1>Редактирование конфигурации</h1>
    @else
        <h1>Создание конфигурации</h1>
    @endif
    <form
            @if(isset($confs))
            action={{route("OperatingConfiguration.Store",['id' => $confs->id])}} method="GET">
        @else
            action={{route("OperatingConfiguration.Store",['id' => '-1'])}} method="GET">
        @endif
        <table>
            <tr>
                <td>Среда</td>
                <td><?php
                    if (isset($confs)) {
                        echo Form::select('envs', $envs, $confs->environment);
                    } else {
                        echo Form::select('envs', $envs);
                    }
                    ?></td>
            </tr>
            <tr>
                <td>SLA</td>
                <td><?php
                    if (isset($confs)) {
                        echo Form::select('slas', $slas, $confs->sla);
                    } else {
                        echo Form::select('slas', $slas);
                    }
                    ?></td>
            </tr>
            <tr>
                <td>Сервер</td>
                <td><?php
                    if (isset($confs)) {
                        echo Form::select('servers', $servers, $confs->server);
                    } else {
                        echo Form::select('servers', $servers);
                    }
                    ?></td>
            </tr>
            <tr>
                <td>Тип памяти</td>
                <td><?php
                    if (isset($confs)) {
                        echo Form::select('typememories', $typeMemories, $confs->typememory);
                    } else {
                        echo Form::select('typememories', $typeMemories);
                    }
                    ?></td>
            </tr>
            <tr>
                <td>Объем диска</td>
                @if(isset($confs))
                    <td><input type="number" name="memoryDisk" value= {{$confs->memorydisk}}></td>
                @else
                    <td><input type="number" name="memoryDisk"></td>
                @endif
            </tr>
            <tr>
                <td>Объем памяти RAM</td>
                @if(isset($confs))
                    <td><input type="number" name="ram" value= {{$confs->ram}}></td>
                @else
                    <td><input type="number" name="ram"></td>
                @endif
            </tr>
            <tr>
                <td>Количество ядер VCPU</td>
                @if(isset($confs))
                    <td><input type="number" name="countCoresVcpu" value= {{$confs->countcoresvcpu}}></td>
                @else
                    <td><input type="number" name="countCoresVcpu"></td>
                @endif
            </tr>
            <tr>
                <td>Мощности</td>
                <td><?php
                    if (isset($confs)) {
                        echo Form::select('performances', $performances, $confs->level);
                    } else {
                        echo Form::select('performances', $performances);
                    }
                    ?></td>
            </tr>
            <tr>
                <td>Стоимость</td>
                @if(isset($confs))
                    <td><input type="float" name="cost" value="{{$confs->cost}}"></td>
                @else
                    <td><input type="float" name="cost"></td>
                @endif
            </tr>
        </table>
        <button type="submit" class="btn btn-primary btn-lg">
            @if(isset($confs))
                Сохранить конфигурацию
            @else
                Создать конфишурацию
            @endif
        </button>
    </form>
@endsection


