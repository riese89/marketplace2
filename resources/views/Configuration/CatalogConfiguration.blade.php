@extends('layouts.MarketPlace')

@section('content')

    <h1> Справочник конфигураций</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Мощность</th>
            <th>Сервер</th>
            <th>Тип памяти</th>
            <th>Память диска</th>
            <th>Объем памяти RAM</th>
            <th>Число ядер VCPU</th>
            <th>Среда</th>
            <th>SLA</th>
            <th>Стоимость</th>
            <th>Действие</th>

            @for($inkr = 0; $inkr < count($confs); $inkr++)
                <tr>
                    <td>{{$confs[$inkr]->Performance()->get()[0]['level']}}</td>
                    <td>{{$confs[$inkr]->Server()->get()[0]['nameServer']}}</td>
                    <td>{{$confs[$inkr]->TypeMemory()->get()[0]['type']}}</td>
                    <td>{{$confs[$inkr]->memorydisk}}</td>
                    <td>{{$confs[$inkr]->ram}}</td>
                    <td>{{$confs[$inkr]->countcoresvcpu}}</td>
                    <td>{{$confs[$inkr]->Environment()->get()[0]['env']}}</td>
                    <td>{{$confs[$inkr]->Sla()->get()[0]['level']}}</td>
                    <td>{{$confs[$inkr]->cost}}</td>
                    <td><a href="{{route("OperatingConfiguration.Edit",['id' => $confs[$inkr]->id])}}"><img
                                    class="imgBtn"
                                    src="http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingConfiguration.Destroy", ['id' => $confs[$inkr]->id])}}"><img
                                    class="imgBtn"
                                    src="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endfor
        </table>
    </div>
    <a href="{{route("OperatingConfiguration.Create")}}">
        <button type="button" class="btn btn-primary btn-lg">Добавить конфигурацию</button>
    </a>
@endsection