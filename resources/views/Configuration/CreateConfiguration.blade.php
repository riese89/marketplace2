@extends('layouts.MarketPlace')

@section('content')
    <h1>Создание конфигурации</h1>
    <form action="{{route("OperatingConfiguration.Store",['id' => "-1"])}}" method="GET">
        <table>
            <tr>
                <td>Название конфигурации</td>
                <td><input type="text" name="configurationName"></td>
            </tr>
            <tr>
                <td>Среда</td>
                <td><?php
                    echo Form::select('envs', $envs);
                    ?>
            </tr>
            <tr>
                <td>SLA</td>
                <td><?php
                    echo Form::select('slas', $slas);
                    ?></td>
            </tr>
            <tr>
                <td>Сервер</td>
                <td><?php
                    echo Form::select('servers', $servers);
                    ?></td>
            </tr>
            <tr>
                <td>Тип памяти</td>
                <td><?php
                    echo Form::select('typememories', $typeMemories);
                    ?></td>
            </tr>
            <tr>
                <td>Объем диска</td>
                <td><input type="number" name="memoryDisk"></td>
            </tr>
            <tr>
                <td>Объем памяти RAM</td>
                <td><input type="number" name="ram"></td>
            </tr>
            <tr>
                <td>Количество ядер VCPU</td>
                <td><input type="number" name="countCoresVcpu"></td>
            </tr>
            <tr>
                <td>Мощности</td>
                <td><?php
                    echo Form::select('performances', $performances);
                    ?></td>
            </tr>
            <tr>
                <td>Стоимость</td>
                <td><input type="float" name="cost" class="form-control" aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default"></td>
            </tr>
        </table>
        <button type="submit" class="btn btn-primary btn-lg">Создать конфигурацию</button>
    </form>
@endsection

