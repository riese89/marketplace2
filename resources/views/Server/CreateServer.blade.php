@extends('layouts.MarketPlace')

@section('content')
    <h1> Создание сервера</h1>
    <form action="{{route("OperatingServer.Store")}}" method="GET">
        Название сервера<br>
        <div class="input-group mb-3">
            <input type="text" name="nameServer" class="form-control" aria-label="Sizing example input"
                   aria-describedby="inputGroup-sizing-default">
        </div>
        Цена<br>
        <div class="input-group mb-3">
            <input type="float" name="costServer" class="form-control" aria-label="Sizing example input"
                   aria-describedby="inputGroup-sizing-default">
        </div>
        Описание сервера<br>
        <div class="input-group mb-3">
            <input type="text" name="descriptionServer" class="form-control" aria-label="Sizing example input"
                   aria-describedby="inputGroup-sizing-default">
        </div>
        <button type="submit">Создать</button>
    </form>
@endsection