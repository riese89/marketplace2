@extends('layouts.MarketPlace')

@section('content')
    <h1>Изменение сервера</h1>
    @foreach($servers as $server)
        <form action="{{route("OperatingServer.Update",['id' => $server->id])}}" method="GET">
            Название<br>
            <div class="input-group mb-3">
                <input type="text" name="nameServer" value="{{$server->nameServer}}" class="form-control"
                       aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            Цена<br>
            <div class="input-group mb-3">
                <input type="float" name="costServer" value="{{$server->costServer}}" class="form-control"
                       aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            Описание<br>
            <div class="input-group mb-3">
                <input type="text" name="descriptionServer" value="{{$server->descriptionServer}}" class="form-control"
                       aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type="submit">Сохранить</button>
        </form>
    @endforeach
@endsection