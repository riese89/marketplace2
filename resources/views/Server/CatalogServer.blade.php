@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник серверов</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Сервер</th>
            <th>Описание</th>
            <th>Цена</th>
            <th>Действие</th>
            @foreach($servers as $server)
                <tr>
                    <td>{{$server->nameServer}}</td>
                    <td>{{$server->descriptionServer}}</td>
                    <td>{{$server->costServer}}</td>
                    <td><a href="{{route("OperatingServer.Edit",['id' => $server->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingServer.Destroy", ['id' => $server->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingServer.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить сервер</button></a>

@endsection