@extends('layouts.MarketPlace')

@section('content')
    <h1> Создание формы</h1>
        <form action = "{{route("OperatingForm.Store")}}" method = "GET">
            <label>Название среды</label><br>
            <div class="input-group mb-3">
                <input type="text" name = "nameEnv" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <label>Описание среды</label><br>
            <div class="input-group mb-3">
                <input type="text" name = "DescrEnv" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type = "submit">Создать</button>
        </form>



@endsection