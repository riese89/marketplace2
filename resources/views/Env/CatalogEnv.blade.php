@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник сред</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Среда</th>
            <th>Описание</th>
            <th>Действие</th>
            @foreach($environments as $environment)
                <tr>
                    <td>{{$environment->env}}</td>
                    <td>{{$environment->description}}</td>
                    <td><a href="{{route("OperatingForm.Edit",['id' => $environment->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingForm.Destroy", ['id' => $environment->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                  </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingForm.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить среду</button></a>

@endsection