@extends('layouts.MarketPlace')

@section('content')
    <h1> Редактирование среды</h1>
    @foreach($envs as $env)
        <form action = "{{route("OperatingForm.Update",['id' => $env->id])}}" method = "GET">
        Название среды<br>
        <div class="input-group mb-3">
            <input type="text" name = "nameEnv" value = "{{$env->env}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <label>Описание среды</label><br>
        <div class="input-group mb-3">
            <input type="text" name = "DescrEnv" value = "{{$env->description}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
            <button type = "submit">Сохранить</button>
        </form>
    @endforeach


@endsection