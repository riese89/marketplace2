@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник типов памяти</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Тип</th>
            <th>Описание</th>
            <th>Действие</th>
            @foreach($types as $type)
                <tr>
                    <td>{{$type->type}}</td>
                    <td>{{$type->description}}</td>
                    <td><a href="{{route("OperatingTypeMemory.Edit",['id' => $type->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingTypeMemory.Destroy", ['id' => $type->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingTypeMemory.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить тип памяти</button></a>

@endsection