@extends('layouts.MarketPlace')

@section('content')
    <h1>Изменение типа памяти</h1>
    @foreach($types as $type)
        <form action = "{{route("OperatingTypeMemory.Update",['id' => $type->id])}}" method = "GET">
            Тип<br>
            <div class="input-group mb-3">
                <input type="text" name = "type" value = "{{$type->type}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            Описание<br>
            <div class="input-group mb-3">
                <input type="text" name = "description" value = "{{$type->description}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type = "submit">Сохранить</button>
        </form>
    @endforeach
@endsection