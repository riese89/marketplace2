@extends('layouts.MarketPlace')

@section('content')
    <h1> Создание типа памяти</h1>
    <form action = "{{route("OperatingTypeMemory.Store")}}" method = "GET">
        Тип<br>
        <div class="input-group mb-3">
            <input type="text" name = "type" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        Описание<br>
        <div class="input-group mb-3">
            <input type="text" name = "description" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <button type = "submit">Создать</button>
    </form>



@endsection