@extends('layouts.MarketPlace')

@section('content')
    <?php
    use App\Model\Environment;
    use App\Model\Bucket;
    ?>
    <h1>Создание заказа</h1>
    <form action="{{route("OperatingOrder.Change")}}" method="GET">

        <?php
        $config_id = Bucket::find($bucket[0]['id'])->bucket_item()->select('config_id')->get();
        $inkr = 0;
        if (count($bucket) == 0)
        ?>
        Стоимость заказа
        @if(count($bucket)==0)
            0
        @else
            {{$bucket[0]['total']}}
        @endif
        $<br>
            Среда
        @foreach ($configurations as $configuration)
            <?php
                    $environment = Environment::where('id',$configuration['environment'])->get()[0]['env'];
            ?>
            {{$environment}}<br>
        @endforeach

    </form>
    <a href="{{route("OperatingOrder.Store",['id' => $bucket[0]['id']])}}">
        <button class="btn btn-primary btn-lg" style="background-color: #6c757d">Создать заказ</button>
    </a>
@endsection

