@extends('layouts.MarketPlace')

@section('content')

    <h1>Создание заказа</h1>
    <form action="{{route("OperatingOrder.Change")}}" method="GET">
        <?php
        $i = 0;
        ?>
        Стоимость заказа @if(count($buckets) != 0)
            {{$buckets[0]['total']}}
        @else
            {{0}}
        @endif
        $<br>
        Среда<br>
        @foreach($nameEnvs as $nameEnv)
            {{$nameEnv->env}}<br>
            <div class="contCreate">
                <div class="card bg-light mb-3" style="max-width: 18rem; margin-right: 4px">
                    <div class="card-header"><br></div>
                    <div class="card-body">
                        <b>ОС</b><br>
                        <b>Мошности</b><br>
                        <b>Данные</b><br>
                        <b>SLA</b><br>
                        <b>Цена</b><br>
                        <b>Количество</b><br>
                    </div>
                </div>
                @foreach($confs as $conf)
                    <?php
                    $check = 0;
                    $count = 0;
                    ?>
                    @if($conf->environment == $nameEnv->id)
                        <div class="card bg-light mb-3" style="max-width: 18rem; margin-right: 4px">
                            @foreach($bucket_items as $bucket_item)
                                @if($bucket_item->config_id == $conf->id)
                                    <div class="card-header"><input type="checkbox" name="configuration{{$i}}"
                                                                    value={{$conf->id}}
                                                                            onChange="this.form.submit()" checked>
                                        Выбрать
                                    </div>
                                    <?php
                                    $check = 1;
                                    $count = $bucket_item->quantity;
                                    $os_id = $bucket_item->oss;
                                    ?>
                                    @break
                                @endif
                            @endforeach
                            @if ($check == 0)
                                <div class="card-header"><input type="checkbox" name="configuration{{$i}}"
                                                                value={{$conf->id}}
                                                                        onChange="this.form.submit()"> Выбрать
                                </div>
                            @endif
                            <div class="card-body">
                                <?php
                                if (isset($os_id)) {
                                    echo Form::select("oss$i", $oss, $os_id) . "<br>";
                                } else {
                                    echo Form::select("oss$i", $oss) . "<br>";
                                }
                                $check = 0;
                                ?>
                                {{$conf->Performance[0]['level']}}<br>
                                {{$conf->TypeMemory[0]['type']}} {{$conf->memorydisk}} GB {{$conf->ram}}
                                MB {{$conf->countcoresvcpu}} ядер<br>
                                {{$conf->Sla[0]['level']}}<br>
                                {{$conf->cost}} $<br>
                                @if($count > 0)
                                    <button name="minus{{$i}}" value="{{$conf->id}}">-</button>
                                @else
                                    <button name="minus{{$i}}" value="{{$conf->id}}" disabled>-</button>
                                @endif
                                {{$count}}
                                <button name="plus{{$i}}" value="{{$conf->id}}">+</button>
                                <br><br>
                            </div>
                        </div>
                        <?php
                        $i++;
                        ?>
                    @endif
                @endforeach
            </div>
        @endforeach
    </form>
    @if(count($buckets)!=0)
        <a href="{{route("OperatingOrder.Store",['id' => $buckets[0]['id']])}}">
            <button class="btn btn-primary btn-lg" style="background-color: #6c757d">Создать заказ</button>
        </a>
    @endif
@endsection

