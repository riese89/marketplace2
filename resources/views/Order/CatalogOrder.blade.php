@extends('layouts.MarketPlace')

@section('content')
    <h1>Личный кабинет</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Название</th>
            <th>Сумма</th>
            @foreach($buckets as $bucket)
                <tr>
                    <td><a href="{{route("OperatingOrder.Show",['id' => $bucket[0]['order_id']])}}">{{$bucket[0]['order_id']}}</a></td>
                    <td>{{$bucket[0]['total']}}$</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
