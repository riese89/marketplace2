@extends('layouts.MarketPlace')

@section('content')
    <b>Номер заказа</b> {{$bucket->order_id}} <br>
    <b>Сумма заказа</b> {{$bucket->total}}$
    <table class="table table-striped table-sm">
        <th>Мощность</th>
        <th>ОС</th>
        <th>Сервер</th>
        <th>Тип памяти</th>
        <th>Память диска</th>
        <th>Объем памяти RAM</th>
        <th>Число ядер VCPU</th>
        <th>Среда</th>
        <th>SLA</th>
        <th>Число</th>
        <th>Общая стоимость</th>
        @for($i=0; $i < count($confs); $i++)
            <tr>
                <td>{{$confs[$i]->Performance[0]['level']}}</td>
                <td>{{$bucket_items[$i]->Os[0]['nameOs']}}</td>
                <td>{{$confs[$i]->Server[0]['nameServer']}}</td>
                <td>{{$confs[$i]->TypeMemory[0]['type']}}</td>
                <td>{{$confs[$i]->memorydisk}}</td>
                <td>{{$confs[$i]->ram}}</td>
                <td>{{$confs[$i]->countcoresvcpu}}</td>
                <td>{{$confs[$i]->Environment[0]['env']}}</td>
                <td>{{$confs[$i]->Sla[0]['level']}}</td>
                <td>{{$bucket_items[$i]->quantity}}</td>
                <td>{{$bucket_items[$i]->quantity * $confs[$i]->cost}}$</td>
            </tr>
        @endfor
    </table>
@endsection