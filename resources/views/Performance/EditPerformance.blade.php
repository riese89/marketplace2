@extends('layouts.MarketPlace')

@section('content')
    <h1>Изменение мощности</h1>
    @foreach($performances as $performance)
        <form action = "{{route("OperatingPerformance.Update",['id' => $performance->id])}}" method = "GET">
            Мощность<br>
            <div class="input-group mb-3">
                <input type="text" name = "level" value = "{{$performance->level}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type = "submit">Сохранить</button>
        </form>
    @endforeach
@endsection