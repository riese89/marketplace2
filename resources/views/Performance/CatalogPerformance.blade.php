@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник мощностей</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>Мощность</th>
            <th>Действие</th>
            @foreach($performances as $performance)
                <tr>
                    <td>{{$performance->level}}</td>
                    <td><a href="{{route("OperatingPerformance.Edit",['id' => $performance->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingPerformance.Destroy", ['id' => $performance->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingPerformance.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить мощность</button></a>



@endsection