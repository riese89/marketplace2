@extends('layouts.MarketPlace')

@section('content')
    <h1>Изменение SLA</h1>
    @foreach($slas as $sla)
        <form action = "{{route("OperatingSla.Update",['id' => $sla->id])}}" method = "GET">
            Уровень<br>
            <div class="input-group mb-3">
                <input type="text" name = "level" value = "{{$sla->level}}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <button type = "submit">Сохранить</button>
        </form>
    @endforeach
@endsection