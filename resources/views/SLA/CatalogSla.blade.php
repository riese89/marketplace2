@extends('layouts.MarketPlace')

@section('content')
    <h1> Справочник SLA</h1>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <th>SLA</th>
            <th>Действие</th>
            @foreach($slas as $sla)
                <tr>
                    <td>{{$sla->level}}</td>
                    <td><a href="{{route("OperatingSla.Edit",['id' => $sla->id])}}"><img class = "imgBtn" src = "http://s1.iconbird.com/ico/2013/6/272/w128h1281371312762PixelKit00451.png"></a>
                        <a href="{{route("OperatingSla.Destroy", ['id' => $sla->id])}}"><img class = "imgBtn" src ="https://maxcdn.icons8.com/Share/icon/Editing//delete1600.png"></a></td>
                </tr>
            @endforeach
        </table>
    </div>
    <a href ="{{route("OperatingSla.Create")}}"><button type="button" class="btn btn-primary btn-lg">Добавить SLA</button></a>



@endsection